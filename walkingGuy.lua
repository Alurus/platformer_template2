   
    walkingGuy = {}


   local sheetOptions =
    {
        width = 64,
        height = 64,
        numFrames = 36,
 
    }






    local sheet_walkingGuy = graphics.newImageSheet( "Sprites/playerSheet.png", sheetOptions )
     -- outlines = graphics.newOutline( 64, sheet_walkingGuy)
    local outlines = graphics.newOutline( 4, "Sprites/playerSheet.png")
    -- sequences table
    -- Sequences -- 
    --This table of sequences is essentially a finite state machine--
    local sequences_walkingGuy = {
        -- consecutive frames sequence
        {
            name = "IdleRight",
            start = 19,
            count = 1,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },

        {
            name = "IdleLeft",
            start = 35,
            count = 1,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },

        {
            name = "walkRight",
            start = 25,
            count = 6,
            time = 450,
            loopCount = 0,
            loopDirection = "bounce"
        },
        {
            name = "jumpRight",
            start = 20,
            count = 1,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },
        {
            name = "jumpLeft",
            start = 34,
            count = 1,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },

        {
            name = "walkLeft",
            start = 1,
            count = 6,
            time = 450,
            loopCount = 0,
            loopDirection = "bounce"
        },
    }
    --Function call--
    local walkingGuy = display.newSprite( sheet_walkingGuy, sequences_walkingGuy )
    physics.addBody( walkingGuy, "dynamic",{density = 9,friction = 1,bounce = 0.2,outline = outlines}) -- density 6


--     return walkingGuy
-- end


    local walkSpeed = 1.2
    --Move player forward in the right direction
    function walkingGuy.movePlayerFwRight()
        walkingGuy:setSequence( "walkRight" )
        walkingGuy:play()
        
    end

    function walkingGuy.movePlayerFwRightStop() 

        walkingGuy:pause()
        --Set the player back to Idle state--
        walkingGuy:setSequence( "IdleRight" )
      
    end

    function walkingGuy.movePlayerFwLeft()
        walkingGuy:setSequence( "walkLeft" )
        walkingGuy:play()
 
    end

    function walkingGuy.movePlayerFwLeftStop() 

        walkingGuy:pause()
        --Set the player back to Idle state--
        walkingGuy:setSequence( "IdleLeft" )       
    end


    function walkingGuy.playerJumpRight()
        walkingGuy:setSequence( "jumpRight" )
        walkingGuy:play()
        -- transition.to( walkingGuy, { time=1600, x = walkingGuy.x } )
    end  

    function walkingGuy.playerJumpLeft()
        walkingGuy:setSequence( "jumpLeft" )
        walkingGuy:play()
        -- transition.to(walkingGuy,{time = 1600,x = walkingGuy.x})
    end




    return walkingGuy




