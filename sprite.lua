
local sprite = {}


    local sheetOptions =
    {
        width = 64,
        height = 64,
        numFrames = 30
    }



 function sprite.getSprite()

    local sheet_walkingGuy = graphics.newImageSheet( "Sprites/playerSheet.png", sheetOptions )
    -- sequences table
    local sequences_walkingGuy = {
        -- consecutive frames sequence
        {
            name = "Idle",
            start = 13,
            count = 1,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },
        {
            name = "walkRight",
            start = 25,
            count = 6,
            time = 450,
            loopCount = 0,
            loopDirection = "bounce"
        },
        {
            name = "jump",
            start = 19,
            count = 5,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },
        {
            name = "walkLeft",
            start = 6,
            count = 6,
            time = 300,
            loopCount = 0,
            loopDirection = "bounce"
        },
    }

    local walkingGuy = display.newSprite( sheet_walkingGuy, sequences_walkingGuy )
    walkingGuy:setSequence( "walkRight" )
    walkingGuy:play()
    walkingGuy = sprite
    return sprite 
end


    return sprite


