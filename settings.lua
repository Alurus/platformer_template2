-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

-------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    local soundOff = self:getObjectByName("SoundOff")
    local musicOff = self:getObjectByName("MusicOff")
    soundOff.isVisible = false
    musicOff.isVisible = false
    local SoundOn = self:getObjectByName("SoundOn")
    local MusicOn = self:getObjectByName("MusicOn")




        SoundButton = self:getObjectByName("SoundButton")
        local sound_On  = true 
        if SoundButton then

            function SoundButton:touch(event)
                local phase = event.phase
                if "ended" == phase then
                    if sound_On ==true then
                        --Sound Off--
                        SoundOn.isVisible = false
                        soundOff.isVisible = true
                        soundOff.x = SoundOn.x
                        soundOff.y = SoundOn.y

                        sound_On  = false
                        --Call some function to turn off sound--
                    elseif sound_On ==false then
                        --Sound On--
                        SoundOn.isVisible = true
                        soundOff.isVisible = false
  

                        sound_On  = true

                        --Call some function to turn on sound--


                    end
                end
            end
            SoundButton:addEventListener( "touch", SoundButton )
        end
        local music_On = true
        MusicButton = self:getObjectByName("MusicButton")
        if MusicButton then

            function MusicButton:touch(event)
                local phase = event.phase
                if "ended" == phase then
                    MusicOn.isVisible = false
                    musicOff.isVisible = true
                    musicOff.x = MusicOn.x
                    musicOff.y = MusicOn.y




                    if music_On ==true then
                        --Sound Off--
                        MusicOn.isVisible = false
                        musicOff.isVisible = true
                        musicOff.x = MusicOn.x
                        musicOff.y = MusicOn.y

                        music_On  = false
                        --Call some function to turn off sound--
                    elseif music_On ==false then
                        --Sound On--
                        MusicOn.isVisible = true
                        musicOff.isVisible = false


                        music_On  = true

                        --Call some function to turn on sound--


                    end
                    
                end
            end
            MusicButton:addEventListener( "touch", MusicButton )
        end



        back = self:getObjectByName("back")
        if back then

            function back:touch(event)
                local phase = event.phase
                if "ended" == phase then
                   
                    composer.gotoScene( "worldMenu", { effect = "fade", time = 300 } )
                end
            end
            back:addEventListener( "touch", back )
        end











end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is off screen and is about to move on screen
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
