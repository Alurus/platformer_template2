-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

-------------------------------------------------------------------------------
local resume = false -- a variable to store the boolian for wether resume is true
function scene:create( event )
    local sceneGroup = self.view

    local menuButton = self:getObjectByName( "Menu" )
    local ResumeButton = self:getObjectByName( "Resume" )
    local RestartButton = self:getObjectByName( "Restart" )


    if ResumeButton then
        function ResumeButton:touch ( event )
            local phase = event.phase
            if "ended" == phase then
                resume = true
                composer.hideOverlay()
            end
        end
        -- add the touch event listener to the button
        ResumeButton:addEventListener( "touch", ResumeButton )
    end
    if menuButton then
        function menuButton:touch ( event )
            local phase = event.phase
            if "ended" == phase then
             
                
                composer.gotoScene( "worldMenu", { effect = "fromRight", time = 0 } )
            end
        end
        -- add the touch event listener to the button
        menuButton:addEventListener( "touch", menuButton )
    end

    if RestartButton then
        function RestartButton:touch ( event )
            local phase = event.phase
            if "ended" == phase then
                -- resume = true
                -- resume = true
                composer.gotoScene("levelRestart", { effect = "fade", time = 0 })
            end
        end
        -- add the touch event listener to the button
        RestartButton:addEventListener( "touch", RestartButton )
    end









end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
        -- Called when the scene is off screen and is about to move on screen
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
    end 
end

function scene:hide( event )
    print("Pause " .. event.name,event.phase) -- A helpful way to debug when scene:hide is fired
    local sceneGroup = self.view
    local phase = event.phase
    local parent = event.parent


    if event.phase == "will" then
        if resume == true then
            parent:onResume()
        end
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
