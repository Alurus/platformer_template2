-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )


-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )


-------------------------------------------------------------------------------
local walkingGuy = require  "walkingGuy" 


function scene:create( event )
    local  sceneGroup = self.view
 

    local bullet = self:getObjectByName("Bullet")
    local playerSensor = self:getObjectByName("PlayerSensor")


    -- playerSensor.gravityScale = 0

    --Get Groups from composer--
    local gameGroup = self:getObjectByName("gameGroup")
    local playerGroup = self:getObjectByName("playerGroup")
    local buttons = self:getObjectByName("ButtonGroup")
     -- playerGroup:insert(walkingGuy)
     --Here--

    --Insert buttons into the stage group for UI/Hud components--
    local stage = display.getCurrentStage()
    stage:insert(buttons)

    local timerMove
    local jumpTimer    
    local hasJumped = false

    --Button Variables--
    local moveFoward = self:getObjectByName("MoveFoward")
    local MoveBackward = self:getObjectByName("MoveBackward")
    local JumpButton = self:getObjectByName("Jump")
    local shootButton = self:getObjectByName("Shoot")

    local ground = self:getObjectByName("Ground")
    local platFormAxel = self:getObjectByName("PlatformAxle")
    local Platform1 = self:getObjectByName("Platform1")




    
    function scene:onResume()
        buttons.isVisible = true
         player.isVisible = true
        physics.start()         
    end

    local function removeButtons()
        --Hide Hud--
        buttons.isVisible = false
        player.isVisible = false
    end


    --Create an instance of the player using the sprite class "walkingGuy"
    player = walkingGuy   
    --Set the player directtion to right as the players intial direction00
    local playerDirection = "Right"
    --Fix the rotation of the player
    player.isFixedRotation = true
    --Set the players position 
    player.isVisible = true
    player.x = 54
    player.y = 235
    playerSensor.x = player.x -10
    playerSensor.y = player.y -20
    playerSensor.isVisible = false
  
    local sensorJoint = physics.newJoint("weld",player,playerSensor,player.x,player.y)



    --Bullet Parameters -- 
    bullet.isBullet = true
    --The bullet will be invisible until we fire it--
    bullet.isVisible = false
    bullet.isFixedRotation = true
    local fired = false


    --Set the platformaxel to be invisible--
    platFormAxel.isVisible = false
    --Connects Platform to axle so that the platform can rotate
    local platformJoint =  physics.newJoint("pivot",Platform1,platFormAxel,platFormAxel.x,platFormAxel.y)

    --Limit rotation enabled
    platformJoint.isLimitEnabled = true
    --Limit the rotation of the platform to 12 degrees so platform so the player can stand on it
    platformJoint:setRotationLimits( -12, 12)
    


    local walkSpeed = 1.2
    local function movePlayerRight()
        player.x = player.x + walkSpeed
    end


    local function movePlayerLeft()
        player.x = player.x -  walkSpeed
    end

    local function movePlayerFwRight(event)
        local phase = event.phase
        if "began" == phase then    
            
            Runtime:addEventListener( "enterFrame", movePlayerRight )
            walkingGuy.movePlayerFwRight()
        elseif "ended" == phase then

            Runtime:removeEventListener( "enterFrame", movePlayerRight )
            walkingGuy.movePlayerFwRightStop()
            playerDirection = "Right"
        end
    end

    local function movePlayerFwLeft(event)
        local phase = event.phase 
        if "began" == phase  then

        
            Runtime:addEventListener( "enterFrame", movePlayerLeft )
            walkingGuy.movePlayerFwLeft()
            
        elseif "ended" == phase then
       
            Runtime:removeEventListener( "enterFrame", movePlayerLeft )
            walkingGuy.movePlayerFwLeftStop()
            playerDirection = "Left"
        end
    end
 
  
    local function playerJump(event)
        local phase = event.phase 
        if phase == "began" then

            if playerDirection == "Right" then --If the player is facing right,jump using the jumpRight sprite--
                
                if hasJumped == false then 
               
                    player:applyLinearImpulse( 0, -40,player.x, player.y )
                    walkingGuy.playerJumpRight()
                    hasJumped=true                    
                    local function onGroundCollision(self,event)
                        local phase = event.phase 
                        if phase == "began" then
                            walkingGuy.movePlayerFwRightStop()
                            hasJumped = false       
                        elseif phase == "ended" then
                                                                                         
                        end
                        return true
                    end

                    player.collision = onGroundCollision
                    player:addEventListener("collision",player)
                end


       
            elseif playerDirection == "Left" then --If the player is facing Left,jump using the jumpLeft sprite--
                
                if hasJumped == false then 
               
                    player:applyLinearImpulse( 0, -40,player.x, player.y )
                    walkingGuy.playerJumpLeft()
                    hasJumped=true                    
                    local function onGroundCollision(self,event)
                        local phase = event.phase 
                        if phase == "began" then
                            walkingGuy.movePlayerFwLeftStop()
                            hasJumped = false       
                        elseif phase == "ended" then                                                      
                              
                        end
                        return true
                    end

                    player.collision = onGroundCollision
                    player:addEventListener("collision",player)
                end
            end
        end
    end



    function bullet:shoot()

        if fired == false then
            local rotation = math.pi*player.rotation/180       
            local force = 124
            local focre2 = -124
            local sinA,cosA = math.sin(rotation),math.cos(rotation)
            --Upon Firing make bullet visible--
            bullet.isVisible = true
            bullet.isFixedRotation = true
            bullet.gravityScale = 1
            if playerDirection == "Left" then -- If the player is facing left,shoot left.Notice the minus 
                self.x = player.x - 8.5*cosA
                self.y = player.y - 8.5*sinA
            elseif playerDirection == "Right" then -- else if the player is facing right,shoot left.Notice the plus 
                self.x = player.x + 8.5*cosA
                self.y = player.y + 8.5*sinA
            end
            self:applyForce( force*cosA,force*sinA, self.x, self.y )
            fired = true
        --If fired,began reloading.
        elseif fired == true then 
            local function reload()
                fired = false
            end
            --This calls the reload function,the player can fire again after 1200ms--
            timer.performWithDelay( 1200, reload ,1 )
        end
    end

    
    moveFoward:addEventListener( "touch", movePlayerFwRight)
    MoveBackward:addEventListener( "touch", movePlayerFwLeft)
    JumpButton:addEventListener( "touch", playerJump)

    function shootButton:touch(event)
        if event.phase == "ended" then
            bullet:shoot() 
            return true
        end
    end
    shootButton:addEventListener( "touch", shootButton)

 

    -- Camera follows bolder automatically
    local function moveCamera()          
        --If the players y position moves past 220,move the game groups position up 220 pixels relative to the players position 
        if (player.y < 220 ) then
            sceneGroup.y = -player.y + 220

        end
    end

    Runtime:addEventListener( "enterFrame", moveCamera )


    pauseButton = self:getObjectByName( "pauseButton" )
    if pauseButton then
            -- touch listener for the button
        function pauseButton:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                     -- Options table for the overlay scene "pause.lua"
                local options = {
                    isModal = true,
                    effect = "fade",
                    time = 400
                }
                -- Runtime:removeEventListener( "enterFrame", moveCamera )
                removeButtons()
                composer.showOverlay("pauseLevel", options)                  
                physics.pause()  
                    
                end
        end
            -- add the touch event listener to the button
            pauseButton:addEventListener( "touch", pauseButton )
    end



    WinButton = self:getObjectByName( "WinButton" )
    if WinButton then
            -- touch listener for the button
        function WinButton:touch ( event )
                local phase = event.phase
                if "ended" == phase then

                    
                    composer.gotoScene( "youWon", { effect = "fromRight", time = 280 } )
                    
                end
        end
            -- add the touch event listener to the button
            WinButton:addEventListener( "touch", WinButton )
    end

    LoseButton = self:getObjectByName( "LoseButton" )
    if LoseButton then
            -- touch listener for the button
        function LoseButton:touch ( event )
                local phase = event.phase
                if "ended" == phase then

                   
                    composer.gotoScene( "youLost", { effect = "fromRight", time = 280 } )
                    
                end
        end
            -- add the touch event listener to the button
            LoseButton:addEventListener( "touch", LoseButton )
    end







end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then


    --GetVariables from composer
    
    


    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
       -- composer.removeScene( "level", false )

        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
